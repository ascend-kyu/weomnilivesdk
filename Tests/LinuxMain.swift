import XCTest

import WeOmniLiveSDKTests

var tests = [XCTestCaseEntry]()
tests += WeOmniLiveSDKTests.allTests()
XCTMain(tests)
