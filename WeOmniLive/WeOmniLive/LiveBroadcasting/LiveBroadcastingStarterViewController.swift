//
//  LiveBroadcastingStarterViewController.swift
//  WeOmniLive
//
//  Created by Chayanon Ardkham on 2/3/21.
//

import Foundation
import UIKit
import WLKCoreKit
import WLKSellerKit

class LiveBroadcastingStarterViewController: UIViewController {
    
    @IBOutlet weak var rtmpUrlTextField: UITextField!
    @IBOutlet weak var streamKeyTextField: UITextField!
    @IBOutlet weak var startButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        startButton.layer.cornerRadius = 8
    }
    
    @IBAction func didTapOnStartButton(_ sender: Any) {
        if let rtmpUrlString = rtmpUrlTextField.text, let streamKey = streamKeyTextField.text {
            let viewController = WLKCoreKit.wlkCoreKit().initBroadcastingViewController(toRTMPServerAt: rtmpUrlString, withStreamKey: streamKey)
            navigationController?.pushViewController(viewController, animated: true)
        }
    }
}
