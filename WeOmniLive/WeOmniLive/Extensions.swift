//
//  Extensions.swift
//  WeOmniLive
//
//  Created by Chayanon Ardkham on 4/3/21.
//

import Foundation
import UIKit

// MARK: - Hide keyboard when tapping outside input field
extension UIViewController {
    
    ///Make ViewController automatically end editing when user tapped outside input field.
    public func hideKeyboardWhenTappedAround() {
        
        let tapGesture = UITapGestureRecognizer(target: self,
                         action: #selector(hideKeyboard))
        tapGesture.cancelsTouchesInView = false
        
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc private func hideKeyboard() {
        
        view.endEditing(true)
    }
}
