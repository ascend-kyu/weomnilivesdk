//
//  AppDelegate.swift
//  WeOmniLive
//
//  Created by Chayanon Ardkham on 1/3/21.
//

import UIKit
import WLKCoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        WLKCoreKit.wlkCoreKit().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        WLKCoreKit.wlkCoreKit().applicationDidBecomeActive(application)
    }
}

