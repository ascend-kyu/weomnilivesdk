//
//  LiveStreamingRoomListViewController.swift
//  WeOmniLive
//
//  Created by Chayanon Ardkham on 4/3/21.
//

import Foundation
import UIKit
import WLKCoreKit
import WLKBuyerKit

class LiveStreamingRoomListViewController: UIViewController {
    
    var roomListView: WLKStreamingRoomListView?
    
    @IBOutlet weak var viewWidthTextField: UITextField!
    @IBOutlet weak var viewHeightTextField: UITextField!
    @IBOutlet weak var scrollDirectionSelectorSegmentedControl: UISegmentedControl!
    @IBOutlet weak var previewContainerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        previewContainerView.backgroundColor = .systemGreen
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if roomListView == nil {
            updateView()
        }
    }
    
    @IBAction func didScrollDirectionSelectorSegmentedControlValueChanged(_ sender: Any) {
        updateView()
    }
    
    @IBAction func didViewWidthTextFieldEditingEnded(_ sender: Any) {
        updateView()
    }
    
    @IBAction func didViewHeightTextFieldEditingEnded(_ sender: Any) {
        updateView()
    }
    
    func updateView() {
        if let widthInt = Int(viewWidthTextField.text ?? ""),
           let heightInt = Int(viewHeightTextField.text ?? "") {
            
            let width = CGFloat(widthInt)
            let height = CGFloat(heightInt)
            let scrollDirection: UICollectionView.ScrollDirection = scrollDirectionSelectorSegmentedControl.selectedSegmentIndex == 0 ? .horizontal : .vertical
            
            generateRoomListView(width: width, height: height, scrollDirection: scrollDirection)
        }
    }
    
    func generateRoomListView(width: CGFloat, height: CGFloat, scrollDirection: UICollectionView.ScrollDirection) {
        
        roomListView?.removeFromSuperview()
        roomListView = nil
        
        roomListView = WLKStreamingRoomListView()
        
        if let roomListView = roomListView {
            
            roomListView.initWLKStreamingRoomListView(streamingRooms: generateMockedStreamingRooms(), scrollDirection: scrollDirection)
            roomListView.delegate = self
            
            let frame = CGRect(x: previewContainerView.frame.width / 2 - width / 2, y: previewContainerView.frame.height / 2 - height / 2, width: width, height: height)
            roomListView.frame = frame
            previewContainerView.addSubview(roomListView)
        }
    }
    
    func generateMockedStreamingRooms() -> [WLKStreamingRoom] {
        var streamingRooms = [WLKStreamingRoom]()
        
        streamingRooms.append(WLKStreamingRoom(title: "Tears of Steel m3u8", hlsUrl: "http://demo.unified-streaming.com/video/tears-of-steel/tears-of-steel.ism/.m3u8"))
        streamingRooms.append(WLKStreamingRoom(title: "Big Buck Bunny VOD m3u8", hlsUrl: "https://multiplatform-f.akamaihd.net/i/multi/will/bunny/big_buck_bunny_,640x360_400,640x360_700,640x360_1000,950x540_1500,.f4v.csmil/master.m3u8"))
        streamingRooms.append(WLKStreamingRoom(title: "Sintel VOD m3u8", hlsUrl: "https://multiplatform-f.akamaihd.net/i/multi/april11/sintel/sintel-hd_,512x288_450_b,640x360_700_b,768x432_1000_b,1024x576_1400_m,.mp4.csmil/master.m3u8"))
        streamingRooms.append(WLKStreamingRoom(title: "Sample from Apple", hlsUrl: "http://devimages.apple.com/iphone/samples/bipbop/bipbopall.m3u8"))
        streamingRooms.append(WLKStreamingRoom(title: "fMP4 m3u8", hlsUrl: "https://devstreaming-cdn.apple.com/videos/streaming/examples/img_bipbop_adv_example_fmp4/master.m3u8"))
        streamingRooms.append(WLKStreamingRoom(title: "Live Akamai m3u8 1", hlsUrl: "https://cph-p2p-msl.akamaized.net/hls/live/2000341/test/master.m3u8"))
        streamingRooms.append(WLKStreamingRoom(title: "Live Akamai m3u8 2", hlsUrl: "https://moctobpltc-i.akamaihd.net/hls/live/571329/eight/playlist.m3u8"))
        streamingRooms.append(WLKStreamingRoom(title: "Dolby VOD m3u8 – this is served over HTTP, so it might result in media errors.", hlsUrl: "http://d3rlna7iyyu8wu.cloudfront.net/skip_armstrong/skip_armstrong_stereo_subs.m3u8"))
        streamingRooms.append(WLKStreamingRoom(title: "Dolby Multichannel m3u8 – this is served over HTTP, so it might result in media errors.", hlsUrl: "http://d3rlna7iyyu8wu.cloudfront.net/skip_armstrong/skip_armstrong_multichannel_subs.m3u8"))
        streamingRooms.append(WLKStreamingRoom(title: "Dolby Multilanguage m3u8 – this is served over HTTP, so it might result in media errors.", hlsUrl: "http://d3rlna7iyyu8wu.cloudfront.net/skip_armstrong/skip_armstrong_multi_language_subs.m3u8"))
        streamingRooms.append(WLKStreamingRoom(title: "Dolby Vision Profile 5 – Dolby Atmos m3u8", hlsUrl: "http://media.developer.dolby.com/DolbyVision_Atmos/profile5_HLS/master.m3u8"))
        streamingRooms.append(WLKStreamingRoom(title: "Azure HLSv4 m3u8 – this is served over HTTP, so it might result in media errors.", hlsUrl: "http://amssamples.streaming.mediaservices.windows.net/91492735-c523-432b-ba01-faba6c2206a2/AzureMediaServicesPromo.ism/manifest(format=m3u8-aapl)"))
        streamingRooms.append(WLKStreamingRoom(title: "Azure HLSv4 m3u8 – this is served over HTTP, so it might result in media errors.", hlsUrl: "http://amssamples.streaming.mediaservices.windows.net/69fbaeba-8e92-4740-aedc-ce09ae945073/AzurePromo.ism/manifest(format=m3u8-aapl)"))
        streamingRooms.append(WLKStreamingRoom(title: "Azure 4K HLSv4 m3u8 – this is served over HTTP, so it might result in media errors.", hlsUrl: "http://amssamples.streaming.mediaservices.windows.net/634cd01c-6822-4630-8444-8dd6279f94c6/CaminandesLlamaDrama4K.ism/manifest(format=m3u8-aapl)"))
        
        return streamingRooms
    }
}

extension LiveStreamingRoomListViewController: WLKStreamingRoomListViewDelegate {
    func wlkStreamingRoomListView(_ wlkStreamingRoomListView: WLKStreamingRoomListView, didSelectStreamingRoom streamingRoom: WLKStreamingRoom) {
        let viewController = WLKCoreKit.wlkCoreKit().initStreamingViewController(streamingRoom: streamingRoom)
        viewController.delegate = self
        navigationController?.pushViewController(viewController, animated: true)
    }
}

extension LiveStreamingRoomListViewController: WLKStreamingViewControllerDelegate {
    func wlkStreamingViewController(_ viewController: WLKStreamingViewController, didSelectProductWith product: Any) {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "ProductDetailView")
        navigationController?.pushViewController(viewController, animated: true)
    }
}
