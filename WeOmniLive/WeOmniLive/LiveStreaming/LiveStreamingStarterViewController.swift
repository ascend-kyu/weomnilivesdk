//
//  LiveStreamingStarterViewController.swift
//  WeOmniLive
//
//  Created by Chayanon Ardkham on 2/3/21.
//

import Foundation
import UIKit
import WLKCoreKit
import WLKBuyerKit

class LiveStreamingStarterViewController: UIViewController {
    
    @IBOutlet private weak var overLayWebViewUrlTextView: UITextField!
    @IBOutlet private weak var mediaUrlTextView: UITextField!
    @IBOutlet private weak var startButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        
        startButton.layer.cornerRadius = 8
    }
    
    @IBAction func didTapOnStartButton(_ sender: Any) {
        if let mediaUrlString = mediaUrlTextView.text {

            let viewController = WLKCoreKit.wlkCoreKit().initStreamingViewController(title: "Manually Provided URL" , hlsUrl: mediaUrlString)
            viewController.overlayWebViewUrlString = overLayWebViewUrlTextView.text
            viewController.delegate = self
            navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

extension LiveStreamingStarterViewController: WLKStreamingViewControllerDelegate {
    func wlkStreamingViewController(_ viewController: WLKStreamingViewController, didSelectProductWith product: Any) {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "ProductDetailView")
        navigationController?.pushViewController(viewController, animated: true)
    }
}
