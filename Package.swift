// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "WeOmniLive",
    platforms: [
        .iOS("11.0")
    ],
    products: [
        .library(
            name: "WLKCoreKit",
            targets: ["WLKCoreKit"]),
        .library(
            name: "WLKBuyerKit",
            targets: ["WLKBuyerKit"]),
        .library(
            name: "WLKSellerKit",
            targets: ["WLKSellerKit"]),
    ],
    dependencies: [
        .package(name: "HaishinKit", url: "https://github.com/shogo4405/HaishinKit.swift", .upToNextMajor(from: "1.1.2")),
    ],
    targets: [
        .target(
            name: "WLKCoreKit",
            dependencies: [
            ],
            path: "Sources/WLKCoreKit"
        ),
        .target(
            name: "WLKBuyerKit",
            dependencies: [
                "WLKCoreKit",
            ],
            path: "Sources/WLKBuyerKit"
        ),
        .target(
            name: "WLKSellerKit",
            dependencies: [
                "WLKCoreKit",
                "HaishinKit",
            ],
            path: "Sources/WLKSellerKit"
        ),
    ]
)
