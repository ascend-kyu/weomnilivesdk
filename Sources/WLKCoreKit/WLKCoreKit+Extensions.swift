//
//  WLKCoreKit+Extensions.swift
//  
//
//  Created by Chayanon Ardkham on 4/3/21.
//

import Foundation
import AVFoundation
import UIKit

extension UICollectionView {
    
    ///Register reusable cells by ClassName.
    public func register<T: UICollectionViewCell>(_ cell: T.Type) {
        register(UINib(nibName: String(describing: cell), bundle: Bundle.module), forCellWithReuseIdentifier: String(describing: cell))
    }
    
    ///Dequeue reusable cells by ClassName.
    public func dequeueReusableCell<T: UICollectionViewCell>(_ cell: T.Type, indexPath: IndexPath) -> T {
        return dequeueReusableCell(withReuseIdentifier: String(describing: cell), for: indexPath) as! T
    }
}

extension UIView {
    
    @discardableResult
    public func initFromNib<T: UIView>() -> T? {
        guard let contentView = Bundle.module.loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)?.first as? T else {
            return nil
        }
        return contentView
    }
}

extension AVPlayer {
    
    public var isPlaying: Bool {
        return timeControlStatus == .playing
    }
    
    public var isLive: Bool {
        return currentItem?.duration == .indefinite
    }
    
    public var currentTimeInSeconds: Int {
        return currentItem?.currentTime().toInt() ?? 0
    }
    
    public var durationInSeconds: Int {
        return currentItem?.duration.toInt() ?? 0
    }
    
    public func seekToLiveIfAble() {
        
        if isLive,
           let currentItem = currentItem,
           let seekableTimeRange = currentItem.seekableTimeRanges.last {
            
            let seekableRange = seekableTimeRange.timeRangeValue
            let seekableStart = CMTimeGetSeconds(seekableRange.start)
            let seekableDuration = CMTimeGetSeconds(seekableRange.duration)
            let livePosition = seekableStart + seekableDuration
            
            let liveTime = CMTime(seconds: livePosition, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
            seek(to: liveTime)
            play()
        }
    }
}

extension CMTime {
    public func toInt() -> Int {
        return seconds.isNaN || seconds.isInfinite ? 0 : Int(seconds)
    }
}

extension UIApplication {
    
    public class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
            if let navigationController = controller as? UINavigationController {
                return topViewController(controller: navigationController.visibleViewController)
            }
            if let tabController = controller as? UITabBarController {
                if let selected = tabController.selectedViewController {
                    return topViewController(controller: selected)
                }
            }
            if let presented = controller?.presentedViewController {
                return topViewController(controller: presented)
            }
            return controller
        }
}
