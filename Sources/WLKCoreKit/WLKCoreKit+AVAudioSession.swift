//
//  WLKCoreKit+AVAudioSession.swift
//  
//
//  Created by Chayanon Ardkham on 9/3/21.
//

import Foundation
import AVFoundation

extension WLKCoreKit {
    
    /// Configure AVAudioSession to be ready for live streaming.
    public func configureAVAudioSessionForLiveStreaming() {
        let session = AVAudioSession.sharedInstance()
        previousAVAudioSessionCategory = session.category
        previousAVAudioSessionCategoryOptions = session.categoryOptions
        previousAVAudioSessionMode = session.mode
        
        do {
            if #available(iOS 10.0, *) {
                try session.setCategory(.playback, mode: .moviePlayback, options: [])
            } else {
                try session.setCategory(.playback)
                try session.setMode(.moviePlayback)
            }
            try session.setActive(true)
        } catch {
            print(error)
        }
    }
    
    /// Configure AVAudioSession to be ready for live broadcasting.
    public func configureAVAudioSessionForLiveBroadcasting() {
        let session = AVAudioSession.sharedInstance()
        previousAVAudioSessionCategory = session.category
        previousAVAudioSessionCategoryOptions = session.categoryOptions
        previousAVAudioSessionMode = session.mode
        
        do {
            if #available(iOS 10.0, *) {
                try session.setCategory(.playAndRecord, mode: .default, options: [.defaultToSpeaker, .allowBluetooth])
            } else {
                session.perform(NSSelectorFromString("setCategory:withOptions:error:"), with: AVAudioSession.Category.playAndRecord, with: [
                    AVAudioSession.CategoryOptions.allowBluetooth,
                    AVAudioSession.CategoryOptions.defaultToSpeaker]
                )
                try session.setMode(.default)
            }
            try session.setActive(true)
        } catch {
            print(error)
        }
    }
    
    /// Restore AVAudioSession back from live streaming session.
    public func restoreToPreviousAVAudioSessionSettings() {
        let session = AVAudioSession.sharedInstance()
        do {
            if #available(iOS 10.0, *) {
                try session.setCategory(previousAVAudioSessionCategory, mode: previousAVAudioSessionMode, options: previousAVAudioSessionCategoryOptions)
            } else {
                session.perform(NSSelectorFromString("setCategory:withOptions:error:"), with: previousAVAudioSessionCategory, with: previousAVAudioSessionCategoryOptions)
                try session.setMode(previousAVAudioSessionMode)
            }
            try session.setActive(true)
        } catch {
            print(error)
        }
    }
}
