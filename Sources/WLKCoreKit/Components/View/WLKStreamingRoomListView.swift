//
//  WLKStreamingRoomListView.swift
//  
//
//  Created by Chayanon Ardkham on 4/3/21.
//

import Foundation
import UIKit

// MARK: - DELEGATION
public protocol WLKStreamingRoomListViewDelegate: class {
    func wlkStreamingRoomListView(_ wlkStreamingRoomListView: WLKStreamingRoomListView, didSelectStreamingRoom streamingRoom: WLKStreamingRoom)
}

// MARK: CLASS
public class WLKStreamingRoomListView: UIView {
    
    // MARK: MODEL
    public weak var delegate: WLKStreamingRoomListViewDelegate?
    
    // MARK: PRIVATE MODEL
    private var streamingRooms = [WLKStreamingRoom]()
    private var refreshControl: UIRefreshControl!
    
    // MARK: VIEW
    @IBOutlet public weak var collectionView: UICollectionView!
    
    // MARK: VIEW LIFE CYCLE
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    func commonInit() {
        guard let view = initFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
    }
    
    // MARK: INITIALIZATION
    public func initWLKStreamingRoomListView(streamingRooms: [WLKStreamingRoom], scrollDirection: UICollectionView.ScrollDirection) {
        self.streamingRooms = streamingRooms
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = scrollDirection
        }
        
        // Pull to refresh
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        collectionView.refreshControl = scrollDirection == .vertical ? refreshControl : nil
        
        collectionView.register(WLKStreamingRoomCollectionViewCell.self)
        collectionView.reloadData()
    }
    
    // MARK: ACTIONS
    @objc func refresh(_ sender: AnyObject) {
        
        // Refresh here
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [self] in
            collectionView.reloadData()
            refreshControl.endRefreshing()
        }
    }
    
    private func displayAlert(title: String) {
        if let viewController = UIApplication.topViewController() {
            let alert = UIAlertController(title: title, message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            viewController.present(alert, animated: true, completion: nil)
        }
    }
}

// MARK: - UICOLLECTIONVIEW DELEGATE, UICOLLECTIONVIEW DATASOURCE, UICOLLECTIONVIEW DELEGATE FLOW LAYOUT
extension WLKStreamingRoomListView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return streamingRooms.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let streamingRoom = streamingRooms[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(WLKStreamingRoomCollectionViewCell.self, indexPath: indexPath)
        cell.initWLKStreamingRoomCollectionViewCell(streamingRoom: streamingRoom)
        
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let streamingRoom = streamingRooms[indexPath.row]
        
        delegate?.wlkStreamingRoomListView(self, didSelectStreamingRoom: streamingRoom)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let scrollDirection = layout.scrollDirection
        
        if scrollDirection == .horizontal {
            return CGSize(width: frame.height / 4 * 3, height: frame.height)
        } else {
            let possibleWidth = floor((frame.width - layout.minimumInteritemSpacing - layout.sectionInset.left - layout.sectionInset.right) / 2)
            return CGSize(width: possibleWidth, height: possibleWidth / 3 * 4)
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == streamingRooms.count - 1 {
            
            // Load more here
            displayAlert(title: "Load more triggered!")
        }
    }
}
