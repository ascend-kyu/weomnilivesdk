//
//  WLKAVPlayerControlView.swift
//  
//
//  Created by Chayanon Ardkham on 8/3/21.
//

import Foundation
import AVFoundation
import UIKit

// MARK: - DELEGATION
public protocol WLKAVPlayerControlViewDelegate: class {
    
}

// MARK: - CLASS
public class WLKAVPlayerControlView: UIView {
    
    // MARK: MODEL
    public weak var delegate: WLKAVPlayerControlViewDelegate?
    public var player: AVPlayer?
    
    // MARK: PRIVATE MODEL
    private var playerTimeObserver: Any?
    private var playerTimeControlStatusObserver: NSKeyValueObservation?
    
    // MARK: VIEW
    @IBOutlet private weak var playAndPauseButton: UIButton!
    @IBOutlet private weak var currentTimeLabel: UILabel!
    @IBOutlet private weak var remainingTimeLabel: UILabel!
    @IBOutlet private weak var seekSlider: UISlider!
    @IBOutlet private weak var liveBadgeView: WLKLiveBadgeView!
    
    // MARK: VIEW LIFE CYCLE
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func commonInit() {
        guard let view = initFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        
        var thumbImage: UIImage?
        if #available(iOS 13.0, *) {
            thumbImage = UIImage(systemName: "circle.fill")
        }
        
        seekSlider.setThumbImage(thumbImage, for: .normal)
        seekSlider.setThumbImage(thumbImage, for: .highlighted)
        
        liveBadgeView.clipsToBounds = true
        liveBadgeView.layer.cornerRadius = 4
        
        isHidden = true
    }
    
    // MARK: INITIALIZATION
    public func initWLKAVPlayerControlView(player: AVPlayer) {
        self.player = player
        
        seekSlider.addTarget(self, action: #selector(seekSliderDidBeginEditing), for: [.touchDown])
        seekSlider.addTarget(self, action: #selector(seekSliderDidEndEditing), for: [.touchUpInside, .touchUpOutside])
        
        playerTimeControlStatusObserver = player.observe(\.timeControlStatus) { [self] (player, change) in
            setTimeObserver()
            updatePlayAndPauseButtonImage()
        }
        
        // Loop the player if it's ended
        NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil, queue: nil) { notification in
            if let player = self.player {
                player.seek(to: CMTime.zero)
                player.play()
            }
        }
    }
    
    // MARK: ACTIONS
    @IBAction private func didTapOnPlayAndPauseButton(_ sender: UIButton) {
        if player?.isPlaying ?? false {
            player?.pause()
        } else {
            player?.play()
        }
    }
    
    private func updatePlayAndPauseButtonImage() {
        var image: UIImage?
        if #available(iOS 13.0, *) {
            image = UIImage(systemName: player?.isPlaying ?? false ? "pause.fill" : "play.fill")
        }
        playAndPauseButton.setImage(image, for: .normal)
    }
}

// MARK: - SEEKSLIDER HANDLERS
extension WLKAVPlayerControlView {
    
    @IBAction private func seekSliderValueChanged(sender: UISlider) {
        let time = CMTime(seconds: Double(seekSlider.value), preferredTimescale: CMTimeScale(NSEC_PER_SEC))
        updateTimeLabels(time: time)
    }
    
    @objc private func seekSliderDidBeginEditing(sender: UISlider) {
        if let player = player {
            player.pause()
        }
    }
    
    @objc private func seekSliderDidEndEditing(sender: UISlider) {
        if let player = player {
            let time = CMTime(seconds: Double(seekSlider.value), preferredTimescale: CMTimeScale(NSEC_PER_SEC))
            player.seek(to: time)
            player.play()
        }
    }
}

// MARK: - FUNCTIONS
extension WLKAVPlayerControlView {
    
    private func setTimeObserver() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
            
            // Remove existed time observer
            if let playerTimeObserver = playerTimeObserver {
                player?.removeTimeObserver(playerTimeObserver)
            }
            
            let interval = CMTime(seconds: 1.0, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
            playerTimeObserver = player?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main) { [self] time in
                updateSeekSlider(time: time)
            }
        }
    }
    
    private func updateSeekSlider(time: CMTime) {
        if let player = player {
            seekSlider.maximumValue = Float(player.durationInSeconds)
            seekSlider.value = Float(time.toInt())
            
            seekSliderValueChanged(sender: seekSlider)
        }
    }
    
    private func updateTimeLabels(time: CMTime) {
        if let player = player {
            currentTimeLabel.text = toTimeString(seconds: time.toInt())
            remainingTimeLabel.text = toTimeString(seconds: player.durationInSeconds - time.toInt())
            
            liveBadgeView.isHidden = !player.isLive
            currentTimeLabel.isHidden = player.isLive
            remainingTimeLabel.isHidden = player.isLive
            seekSlider.isHidden = player.isLive
            
            isHidden = false
        }
    }
    
    private func toTimeString(seconds: Int) -> String {
        let h = seconds / 3600
        let m = (seconds % 3600) / 60
        let s = (seconds % 3600) % 60
        return String(format: "%1d:%02d:%02d", h, m, s)
    }
}
