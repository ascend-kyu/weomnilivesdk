//
//  WLKLiveBadgeView.swift
//  
//
//  Created by Chayanon Ardkham on 8/3/21.
//

import Foundation
import UIKit

public class WLKLiveBadgeView: UIView {
    
    // MARK: VIEW LIFE CYCLE
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    func commonInit() {
        guard let view = initFromNib() else { return }
        view.frame = self.bounds
        self.addSubview(view)
    }
}
