//
//  WLKStreamingRoomCollectionViewCell.swift
//  
//
//  Created by Chayanon Ardkham on 4/3/21.
//

import Foundation
import UIKit

public class WLKStreamingRoomCollectionViewCell: UICollectionViewCell {
    
    // MARK: MODEL
    public var streamingRoom: WLKStreamingRoom!
    
    // MARK: VIEW
    @IBOutlet public weak var liveBadgeView: WLKLiveBadgeView!
    @IBOutlet public weak var liveAudienceCountLabel: UILabel!
    @IBOutlet public weak var liveTitleLabel: UILabel!
    
    // MARK: INITIALIZATION
    func initWLKStreamingRoomCollectionViewCell(streamingRoom: WLKStreamingRoom) {
        self.streamingRoom = streamingRoom
        
        liveTitleLabel.text = streamingRoom.title
    }
}
