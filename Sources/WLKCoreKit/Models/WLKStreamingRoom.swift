//
//  WLKStreamingRoom.swift
//  
//
//  Created by Chayanon Ardkham on 5/3/21.
//

import Foundation

public class WLKStreamingRoom {
    public var title: String?
    public var hlsUrl: String?
    
    public convenience init(title: String, hlsUrl: String) {
        self.init()
        
        self.title = title
        self.hlsUrl = hlsUrl
    }
}
