//
//  WLKCoreKit.swift
//  
//
//  Created by Chayanon Ardkham on 4/3/21.
//

import Foundation
import AVFoundation
import UIKit

// MARK: - PROTOCOL
public protocol WLKCoreKitProtocol {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?)
    func applicationDidBecomeActive(_ application: UIApplication)
}

// MARK: - CLASS
public class WLKCoreKit: NSObject, WLKCoreKitProtocol {
    
    // MARK: INSTANCE
    private override init() {}
    private static let shared = WLKCoreKit()
    
    public static func wlkCoreKit() -> WLKCoreKit {
        return shared
    }
    
    // MARK: VARIABLES
    
    // MARK: INTERNAL VARIABLES
    internal var previousAVAudioSessionCategory: AVAudioSession.Category!
    internal var previousAVAudioSessionCategoryOptions: AVAudioSession.CategoryOptions!
    internal var previousAVAudioSessionMode: AVAudioSession.Mode!
    
    // MARK: FUNCTION
    public func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]?) {
        
    }
    
    public func applicationDidBecomeActive(_ application: UIApplication) {
        
        let resumeLiveSelector = NSSelectorFromString("resumeLiveStreamingIfNeeded")
        if responds(to: resumeLiveSelector) {
            perform(resumeLiveSelector)
        }
    }
}
