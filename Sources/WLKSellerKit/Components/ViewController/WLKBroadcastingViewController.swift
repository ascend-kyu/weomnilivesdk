//
//  WLKBroadcastingViewController.swift
//  
//
//  Created by Chayanon Ardkham on 2/3/21.
//

import Foundation
import UIKit
import AVFoundation
import HaishinKit
import VideoToolbox
import WLKCoreKit

// MARK: - LIVE BUTTON STATUS
enum BroadcastingButtonStatus {
    case start
    case connecting
    case stop
    case pause
    case resume
    case attempConnection(attemp: Int)
    
    var title: String {
        switch self {
        case .start:
            return "Go Live!"
        case .connecting:
            return "Connecting..."
        case .stop:
            return "Stop"
        case .pause:
            return "Pause"
        case .resume:
            return "Resume"
        case .attempConnection(let attemp):
            return "Attemp to connect #\(attemp)"
        }
    }
    
    var shouldDisplayLiveBadge: Bool {
        switch self {
        case .start, .connecting, .resume, .attempConnection:
            return false
        default:
            return true
        }
    }
    
    var shouldUpdateLiveBadge: Bool {
        switch self {
        case .connecting, .attempConnection:
            return false
        default:
            return true
        }
    }
}

// MARK: - CLASS
public class WLKBroadcastingViewController: UIViewController {
    
    // MARK: MODEL
    internal var streamKey: String!
    internal var rtmpUrlString: String!
    
    private var currentStartButtonStatus: BroadcastingButtonStatus = .start {
        didSet {
            updateInterface()
        }
    }
    
    // MARK: VIEW
    @IBOutlet private weak var broadcastingView: WLKBroadcastingView!
    @IBOutlet private weak var cameraSelectorSegmentedControl: UISegmentedControl!
    @IBOutlet private weak var muteStatusSelectorSegmentedControl: UISegmentedControl!
    @IBOutlet private weak var broadcastingMainButton: UIButton!
    
    @IBOutlet private weak var backButton: UIButton!
    
    @IBOutlet private weak var liveBadgeView: WLKLiveBadgeView!
    
    // MARK: VIEW LIFE CYCLE
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        liveBadgeView.clipsToBounds = true
        liveBadgeView.layer.cornerRadius = 4
        
        broadcastingMainButton.layer.cornerRadius = 8
        
        broadcastingView.initWLKBroadcastingView(rtmpUrlString: rtmpUrlString, streamKey: streamKey)
        broadcastingView.delegate = self
                
        broadcastingMainButton.setTitle("Go Live!", for: .normal) //
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        tabBarController?.tabBar.isHidden = true
        
        WLKCoreKit.wlkCoreKit().configureAVAudioSessionForLiveBroadcasting()
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        tabBarController?.tabBar.isHidden = false
        
        broadcastingView.stopLiveBroadcasting()
    }
    
    // MARK: - ACTIONS
    @IBAction func didChangeCameraSelectorSegmentedControlValueChanged(_ sender: UISegmentedControl) {
        switch cameraSelectorSegmentedControl.selectedSegmentIndex {
        case 1:
            broadcastingView.setCaptureDevicePosition(position: .front)
        default:
            broadcastingView.setCaptureDevicePosition(position: .back)
        }
    }
    
    @IBAction func didMuteStatusSelectorSegmentedControlValueChanged(_ sender: UISegmentedControl) {
        switch muteStatusSelectorSegmentedControl.selectedSegmentIndex {
        case 1:
            broadcastingView.muteLiveBroadcasting()
        default:
            broadcastingView.unmuteLiveBroadcasting()
        }
    }
    
    @IBAction func didTapOnLiveButton(_ sender: UIButton) {
        switch currentStartButtonStatus {
        case .start:
            broadcastingView.startLiveBroadcasting()
        case .pause:
            broadcastingView.pauseLiveBroadcasting()
        case .resume:
            broadcastingView.resumeLiveBroadcasting()
        default:
            break
        }
    }
    
    @IBAction func didTapOnBackButton(_ sender: Any) {
        broadcastingView.stopLiveBroadcasting()
        
        WLKCoreKit.wlkCoreKit().restoreToPreviousAVAudioSessionSettings()
        
        if let navigationController = navigationController {
            navigationController.popViewController(animated: true)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    private func updateInterface() {
        broadcastingMainButton.setTitle(currentStartButtonStatus.title, for: .normal)
        
        if !currentStartButtonStatus.shouldUpdateLiveBadge {
            return
        }
        
        liveBadgeView.alpha = currentStartButtonStatus.shouldDisplayLiveBadge ? 0 : 1
        backButton.isHidden = false
        
        backButton.alpha = currentStartButtonStatus.shouldDisplayLiveBadge ? 1 : 0
        backButton.isHidden = false
        
        UIView.animate(withDuration: 0.25) { [self] in
            liveBadgeView.alpha = currentStartButtonStatus.shouldDisplayLiveBadge ? 1 : 0
            backButton.alpha = currentStartButtonStatus.shouldDisplayLiveBadge ? 0 : 1
        } completion: { [self] (_) in
            liveBadgeView.isHidden = !currentStartButtonStatus.shouldDisplayLiveBadge
            backButton.isHidden = currentStartButtonStatus.shouldDisplayLiveBadge
        }
    }
}

// MARK: - WLK BROADCASTING VIEW DELEGATE
extension WLKBroadcastingViewController: WLKBroadcastingViewDelegate {
    
    public func wlkBroadcastingViewWillStartLiveBroadcasting(_ wlkBroadcastingView: WLKBroadcastingView) {
        currentStartButtonStatus = .connecting
    }
    
    public func wlkBroadcastingViewDidStartLiveBroadcasting(_ wlkBroadcastingView: WLKBroadcastingView) {
        currentStartButtonStatus = .pause
    }
    
    public func wlkBroadcastingViewDidPauseLiveBroadcasting(_ wlkBroadcastingView: WLKBroadcastingView) {
        currentStartButtonStatus = .resume
    }
    
    public func wlkBroadcastingViewDidResumeLiveBroadcasting(_ wlkBroadcastingView: WLKBroadcastingView) {
        currentStartButtonStatus = .pause
    }
    
    public func wlkBroadcastingView(_ wlkBroadcastingView: WLKBroadcastingView, didConnectionStatusChanged status: RTMPConnection.Code) {
        //
    }
    
    public func wlkBroadcastingView(_ wlkBroadcastingView: WLKBroadcastingView, didAttempToReconnect attemp: Int) {
        currentStartButtonStatus = .attempConnection(attemp: attemp)
    }
}
