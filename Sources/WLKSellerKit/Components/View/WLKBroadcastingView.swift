//
//  WLKBroadcastingView.swift
//  
//
//  Created by Chayanon Ardkham on 4/3/21.
//

import Foundation
import UIKit
import AVFoundation
import HaishinKit
import VideoToolbox

public protocol WLKBroadcastingViewDelegate: class {
    func wlkBroadcastingViewWillStartLiveBroadcasting(_ wlkBroadcastingView: WLKBroadcastingView)
    func wlkBroadcastingViewDidStartLiveBroadcasting(_ wlkBroadcastingView: WLKBroadcastingView)
    func wlkBroadcastingViewDidPauseLiveBroadcasting(_ wlkBroadcastingView: WLKBroadcastingView)
    func wlkBroadcastingViewDidResumeLiveBroadcasting(_ wlkBroadcastingView: WLKBroadcastingView)
    func wlkBroadcastingView(_ wlkBroadcastingView: WLKBroadcastingView, didConnectionStatusChanged status: RTMPConnection.Code)
    func wlkBroadcastingView(_ wlkBroadcastingView: WLKBroadcastingView, didAttempToReconnect attemp: Int)
}

public class WLKBroadcastingView: UIView {
    
    // MARK: MODEL
    public weak var delegate: WLKBroadcastingViewDelegate?
    
    // MARK: PRIVATE MODEL
    private var defaultCamera: AVCaptureDevice.Position = .back
    private var updateTimeInterval = 5
    
    private var streamKey: String!
    private var rtmpUrlString: String!
    
    private var rtmpConnection = RTMPConnection()
    private var rtmpStream: RTMPStream!
    
    private var isCurrentlyLive = false
    
    private var reconnectAttempt = 0
    private var lastBwChange = 0
    
    private let profile: WLKBroadcastingProfile = .sd_360p_30fps_1mbps
    
    private var broadcastingView: MTHKView!
    
    // MARK: INITIALIZATION
    public func initWLKBroadcastingView(rtmpUrlString: String, streamKey: String) {
        
        broadcastingView = MTHKView(frame: bounds)
        broadcastingView.videoGravity = .resizeAspectFill
        addSubview(broadcastingView)
        
        self.rtmpUrlString = rtmpUrlString
        self.streamKey = streamKey
        
        rtmpStream = RTMPStream(connection: rtmpConnection)
        
        configureLiveStreamSettings()
        
        rtmpStream.attachAudio(AVCaptureDevice.default(for: .audio)) { error in
            print(error.description)
        }
        rtmpStream.attachCamera(DeviceUtil.device(withPosition: defaultCamera)) { error in
            print(error.description)
        }

        // TapToFocus Gesture
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTapToFocusGesture(_:)))
        addGestureRecognizer(tap)
        isUserInteractionEnabled = true
        
        broadcastingView.attachStream(rtmpStream)
        
        // RTMP Connection Listeners
        rtmpConnection.addEventListener(.rtmpStatus, selector: #selector(rtmpStatusHandler), observer: self)
        rtmpConnection.addEventListener(.ioError, selector: #selector(rtmpErrorHandler), observer: self)
        
        rtmpStream.delegate = self
    }
    
    // MARK: ACTIONS
    @objc func handleTapToFocusGesture(_ sender: UITapGestureRecognizer) {
        if sender.state == UIGestureRecognizer.State.ended {
            let point = sender.location(in: broadcastingView)
            let pointOfInterest = CGPoint(x: point.x / broadcastingView.bounds.size.width, y: point.y / broadcastingView.bounds.size.height)
            rtmpStream.setPointOfInterest(pointOfInterest, exposure: pointOfInterest)
        }
    }
    
    public func setCaptureDevicePosition(position: AVCaptureDevice.Position) {
        rtmpStream.attachCamera(DeviceUtil.device(withPosition: position))
    }
    
    private func configureLiveStreamSettings() {
        
        // Configure the capture settings from the camera
        rtmpStream.captureSettings = [
            .sessionPreset: AVCaptureSession.Preset.hd1920x1080,
            .continuousAutofocus: true,
            .continuousExposure: true,
            .fps: profile.frameRate
        ]
        
        // Get the orientation of the app, and set the video orientation appropriately
        var videoOrientation: AVCaptureVideoOrientation?
        var isPortrait = true
        if #available(iOS 13.0, *),
           let orientation = UIApplication.shared.windows.first?.windowScene?.interfaceOrientation {
            videoOrientation = DeviceUtil.videoOrientation(by: orientation)
            isPortrait = orientation.isPortrait
        } else {
            let orientation = UIApplication.shared.statusBarOrientation
            videoOrientation = DeviceUtil.videoOrientation(by: orientation)
            isPortrait = orientation.isPortrait
        }
        
        if let videoOrientation = videoOrientation {
            rtmpStream.orientation = videoOrientation
            rtmpStream.videoSettings = [
                .width: (isPortrait) ? profile.height : profile.width,
                .height: (isPortrait) ? profile.width : profile.height,
                .bitrate: profile.bitrate,
                .profileLevel: kVTProfileLevel_H264_Main_AutoLevel,
                .maxKeyFrameIntervalDuration: 2, // 2 seconds
            ]
        }
        
        // Configure the RTMP audio stream
        rtmpStream.audioSettings = [
            .bitrate: 128000 // Always use 128kbps
        ]
    }
    
    public func startLiveBroadcasting() {
        // If RTMP already connect, publish.
        if rtmpConnection.connected {
            publishStream()
        } else {
            connectRTMP()
        }
        
        isCurrentlyLive = true
        delegate?.wlkBroadcastingViewWillStartLiveBroadcasting(self)
    }
    
    public func pauseLiveBroadcasting() {
        isCurrentlyLive = false
        
        rtmpStream.paused = true
        delegate?.wlkBroadcastingViewDidPauseLiveBroadcasting(self)
    }
    
    public func resumeLiveBroadcasting() {
        isCurrentlyLive = true
        
        rtmpStream.paused = false
        delegate?.wlkBroadcastingViewDidResumeLiveBroadcasting(self)
    }
    
    public func stopLiveBroadcasting() {
        rtmpStream.close()
    }
    
    public func muteLiveBroadcasting() {
        rtmpStream.audioSettings[.muted] = true
    }
    
    public func unmuteLiveBroadcasting() {
        rtmpStream.audioSettings[.muted] = false
    }
    
    private func connectRTMP() {
        print("WLKBroadcastingView: Connecting to RTMP server.")
        rtmpConnection.connect(rtmpUrlString)
    }
    
    private func publishStream() {
        print("WLKBroadcastingView: Publishing live broadcasting.")
        rtmpStream.publish(self.streamKey)
        DispatchQueue.main.async {
            self.delegate?.wlkBroadcastingViewDidStartLiveBroadcasting(self)
        }
    }
}

// MARK: - RTMP LISTENERS
extension WLKBroadcastingView {
    
    // RTMPStream or RTMPConnection 's status changed
    @objc
    private func rtmpStatusHandler(_ notification: Notification) {
        let e = Event.from(notification)
        guard let data: ASObject = e.data as? ASObject, let code: String = data["code"] as? String else {
            return
        }
        
        if let statusCode = RTMPConnection.Code(rawValue: code) {
            delegate?.wlkBroadcastingView(self, didConnectionStatusChanged: statusCode)
        }
        
        switch code {
        case RTMPConnection.Code.connectSuccess.rawValue:
            reconnectAttempt = 0
            if isCurrentlyLive {
                publishStream()
            }
        case RTMPConnection.Code.connectFailed.rawValue, RTMPConnection.Code.connectClosed.rawValue:
            print("WLKBroadcastingView: RTMP Connection was not successful.")
            
            if isCurrentlyLive {
                reconnectAttempt += 1
                
                DispatchQueue.main.async {
                    self.delegate?.wlkBroadcastingView(self, didAttempToReconnect: self.reconnectAttempt)
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + TimeInterval(updateTimeInterval)) {
                    self.connectRTMP()
                }
            }
        default:
            break
        }
    }

    // RTMP Error
    @objc
    private func rtmpErrorHandler(_ notification: Notification) {
    }
}

// MARK: - RTMP STREAM DELEGATE
extension WLKBroadcastingView: RTMPStreamDelegate {
    
    public func rtmpStreamDidClear(_ stream: RTMPStream) {
    }
    
    // Statistics
    public func rtmpStream(_ stream: RTMPStream, didStatics connection: RTMPConnection) {
//        DispatchQueue.main.async {
//            self.fpsLabel.text = String(stream.currentFPS) + " fps"
//            self.bitrateLabel.text = String((connection.currentBytesOutPerSecond / 125)) + " kbps"
//        }
    }
    
    // Insufficient bandwidth
    public func rtmpStream(_ stream: RTMPStream, didPublishInsufficientBW connection: RTMPConnection) {
        print("WLKBroadcastingView: Insufficient bandwidth handler triggered.")
        
        if (Int(NSDate().timeIntervalSince1970) - lastBwChange) > updateTimeInterval {
            print("WLKBroadcastingView: Will try to change bitrate.")
            
            // Decrease bitrate by 30%
            let newBitrate = Double(stream.videoSettings[.bitrate] as! UInt32) * Double(0.7)
            print("WLKBroadcastingView: Proposed new bitrate: " + String(newBitrate))
            stream.videoSettings[.bitrate] = newBitrate
            lastBwChange = Int(NSDate().timeIntervalSince1970)
        }
    }
    
    // Sufficient bandwidth
    public func rtmpStream(_ stream: RTMPStream, didPublishSufficientBW connection: RTMPConnection) {
        print("WLKBroadcastingView: Sufficient bandwidth handler triggered.")
        
        if (Int(NSDate().timeIntervalSince1970) - lastBwChange) > updateTimeInterval {
            print("WLKBroadcastingView: Will try to change bitrate.")
            
            // Inscrease bitrate by 30%
            let newBitrate = Double(stream.videoSettings[.bitrate] as! UInt32) * Double(1.3)
            let maximumBitrate = Double(profile.bitrate)
            
            guard newBitrate < maximumBitrate else {
                print("WLKBroadcastingView: Already using maximum bitrite for current profile.")
                return
            }
            
            print("WLKBroadcastingView: Proposed new bitrate: " + String(newBitrate))
            stream.videoSettings[.bitrate] = newBitrate
            lastBwChange = Int(NSDate().timeIntervalSince1970)
        }
    }
}
