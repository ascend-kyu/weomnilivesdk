//
//  WLKSellerKit.swift
//  
//
//  Created by Chayanon Ardkham on 2/3/21.
//

import Foundation
import WLKCoreKit

extension WLKCoreKit {
    
    /// WeOmniLive's Broadcasting View is sdk-provided broadcasting view with multiple functions enabled such as camera-switching, mute-unmute toggler, basic live broadcasting controls, etc. Primarily used for live broadcasting (for seller).
    public func initBroadcastingViewController(toRTMPServerAt rtmpUrlString: String, withStreamKey streamKey: String) -> WLKBroadcastingViewController {
        let viewController = WLKBroadcastingViewController.initFromNib()
        viewController.rtmpUrlString = rtmpUrlString
        viewController.streamKey = streamKey
        return viewController
    }
}
