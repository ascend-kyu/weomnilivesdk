//
//  WLKStreamingViewController.swift
//  
//
//  Created by Chayanon Ardkham on 3/3/21.
//

import Foundation
import UIKit
import AVKit
import AVFoundation
import WebKit
import WLKCoreKit

// MARK: - DELEGATION
public protocol WLKStreamingViewControllerDelegate: class {
    func wlkStreamingViewController(_ viewController: WLKStreamingViewController, didSelectProductWith product: Any)
}

// MARK: - CLASS
public class WLKStreamingViewController: UIViewController {
    
    // MARK: MODEL
    public weak var delegate: WLKStreamingViewControllerDelegate?
    public var overlayWebViewUrlString: String?
    
    public var pictureInPictureController: AVPictureInPictureController?
    
    // MARK: INTERNAL MODEL
    internal var isAutoplayEnabled: Bool = true
    internal var streamingRoom: WLKStreamingRoom!
    
    // MARK: PRIVATE MODEL
    private var player: AVPlayer?
    private var playerLayer: AVPlayerLayer?
    private var blurredPlayerLayer: AVPlayerLayer?
    private var playerObserver: NSKeyValueObservation?
    
    private var isAutoPlayed: Bool = false
    
    // MARK: VIEW
    @IBOutlet private weak var playerContainerView: UIView!
    @IBOutlet private weak var productContainerView: UIView!
    @IBOutlet internal weak var blurredPlayerContainerView: UIView!
    @IBOutlet internal weak var overlayWebView: WKWebView!
    
    @IBOutlet private weak var liveTitleLabel: UILabel!
    @IBOutlet private weak var liveAudienceCountLabel: UILabel!
    
    @IBOutlet private weak var broadcasterProfileImageView: UIImageView!
    @IBOutlet private weak var broadcasterNameLabel: UILabel!
    
    @IBOutlet private weak var playerControlView: WLKAVPlayerControlView!
    
    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!
    
    // MARK: VIEW LIFE CYCLE
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        initWLKStreamingViewController()
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
        pictureInPictureController?.stopPictureInPicture()
        WLKCoreKit.wlkCoreKit().configureAVAudioSessionForLiveStreaming()
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        autoplayLiveStreamingIfAvailable()
        resumeLiveStreamingIfNeeded()
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
        
        pictureInPictureController?.startPictureInPicture()
    }
    
    // MARK: INITIALIZATION
    func initWLKStreamingViewController() {
        
        broadcasterProfileImageView.layer.cornerRadius = broadcasterProfileImageView.frame.height / 2
        
        productContainerView.layer.cornerRadius = 8
        productContainerView.layer.borderWidth = 1
        productContainerView.layer.borderColor = UIColor.white.cgColor
        
        overlayWebView.isOpaque = false
        overlayWebView.backgroundColor = .clear
        overlayWebView.scrollView.backgroundColor = .clear
        
        if let urlString = overlayWebViewUrlString {
            loadWebView(with: urlString)
        }
        
        liveTitleLabel.text = streamingRoom.title
    }
    
    func autoplayLiveStreamingIfAvailable() {
        if isAutoplayEnabled && !isAutoPlayed,
           let urlString = streamingRoom.hlsUrl,
           let mediaUrl = URL(string: urlString) {
            startLiveStreaming(with: mediaUrl)
            isAutoPlayed = true
        }
    }
    
    func startLiveStreaming(with mediaUrl: URL) {
        
        stopLiveStreaming()
        
        player = AVPlayer(url: mediaUrl)
        
        if let player = player {
            
            playerLayer = AVPlayerLayer(player: player)
            playerLayer?.frame = playerContainerView.bounds
            
            if let playerLayer = playerLayer {
                playerContainerView.layer.addSublayer(playerLayer)
                
                pictureInPictureController = AVPictureInPictureController(playerLayer: playerLayer)
                pictureInPictureController?.delegate = self
            }
            
            player.play()
            
            playerControlView.initWLKAVPlayerControlView(player: player)
            
            // Observe when player starts playing
            playerObserver = player.observe(\.timeControlStatus) { [self] (_, _) in
                setIfPictureInPictureControllerRequiresLinearPlayback()
                addBlurredPlayerLayerInBackground()
            }
        }
    }
    
    func stopLiveStreaming() {
        
        if let player = player,
           let playerLayer = playerLayer {
            
            playerLayer.removeFromSuperlayer()
            self.playerLayer = nil
            
            blurredPlayerLayer?.removeFromSuperlayer()
            self.blurredPlayerLayer = nil
            
            player.pause()
            self.player = nil
            
            pictureInPictureController?.stopPictureInPicture()
            pictureInPictureController = nil
        }
    }
    
    func resumeLiveStreamingIfNeeded() {
        if let player = player, player.timeControlStatus == .paused && player.isLive {
            player.seekToLiveIfAble()
        }
    }
    
    private func addBlurredPlayerLayerInBackground() {
        
        blurredPlayerLayer = AVPlayerLayer(player: player)
        if let blurredPlayerLayer = blurredPlayerLayer {
            
            for subview in blurredPlayerContainerView.subviews {
                subview.removeFromSuperview()
            }
            
            blurredPlayerLayer.videoGravity = .resizeAspectFill
            blurredPlayerLayer.frame = blurredPlayerContainerView.bounds
            blurredPlayerContainerView.layer.addSublayer(blurredPlayerLayer)
            
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = blurredPlayerContainerView.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            blurredPlayerContainerView.addSubview(blurEffectView)
        }
    }
    
    @IBAction func didTapOnBackButton(_ sender: Any) {
        stopLiveStreaming()
        
        WLKCoreKit.wlkCoreKit().restoreToPreviousAVAudioSessionSettings()
        
        if let navigationController = navigationController {
            navigationController.popViewController(animated: true)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction private func didTapOnProductButton(_ sender: Any) {
        delegate?.wlkStreamingViewController(self, didSelectProductWith: 0)
    }
}

// MARK: - AVPICTUREINPICTURE CONTROLLER DELEGATE
extension WLKStreamingViewController: AVPictureInPictureControllerDelegate {
    
    private func setIfPictureInPictureControllerRequiresLinearPlayback() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
            if #available(iOS 14.0, *) {
                pictureInPictureController?.requiresLinearPlayback = player?.isLive ?? false
            } else {
                pictureInPictureController?.setValue(player?.isLive ?? false, forKey: "requiresLinearPlayback")
            }
            
            activityIndicatorView.isHidden = true
        }
    }
    
    public func pictureInPictureController(_ pictureInPictureController: AVPictureInPictureController,
                                    restoreUserInterfaceForPictureInPictureStopWithCompletionHandler completionHandler: @escaping (Bool) -> Void) {
        
        navigationController?.popToViewController(self, animated: true)
        
        DispatchQueue.main.async {
            completionHandler(true)
        }
    }
}
