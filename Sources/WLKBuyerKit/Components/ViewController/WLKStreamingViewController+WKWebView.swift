//
//  WLKStreamingViewController+WKWebView.swift
//  
//
//  Created by Chayanon Ardkham on 5/3/21.
//

import Foundation
import UIKit
import WebKit

// MARK: - WEBVIEW HANDLER
extension WLKStreamingViewController {
    
    internal func loadWebView(with urlString: String) {
        if let url = URL(string: urlString) {
            let request = URLRequest(url: url)
            overlayWebView.load(request)
        }
    }
}
