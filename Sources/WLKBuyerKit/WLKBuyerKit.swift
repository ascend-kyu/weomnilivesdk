//
//  WLKBuyerKit.swift
//  
//
//  Created by Chayanon Ardkham on 1/3/21.
//

import Foundation
import UIKit
import WLKCoreKit

extension WLKCoreKit {
    
    /// WeOmniLive's Streaming View is sdk-provided streaming view with multiple functions enabled. Primarily used for live streaming (for buyer).
    public func initStreamingViewController(title: String, hlsUrl: String, isAutoplayEnabled: Bool = true) -> WLKStreamingViewController {
        return initStreamingViewController(streamingRoom: WLKStreamingRoom(title: title, hlsUrl: hlsUrl))
    }
    
    /// WeOmniLive's Streaming View is sdk-provided streaming view with multiple functions enabled. Primarily used for live streaming (for buyer).
    public func initStreamingViewController(streamingRoom: WLKStreamingRoom, isAutoplayEnabled: Bool = true) -> WLKStreamingViewController {
        let viewController = WLKStreamingViewController.initFromNib()
        viewController.streamingRoom = streamingRoom
        viewController.isAutoplayEnabled = isAutoplayEnabled
        return viewController
    }
    
    @objc internal func resumeLiveStreamingIfNeeded() {
        if let viewController = UIApplication.topViewController() as? WLKStreamingViewController {
            viewController.pictureInPictureController?.stopPictureInPicture()
            viewController.resumeLiveStreamingIfNeeded()
        }
    }
}
